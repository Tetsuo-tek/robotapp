#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <Core/App/qtskappctrl.h>

#include "robotsatqml.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    const QUrl url(u"qrc:/RobotMobileApp/Main.qml"_qs);

    QObject::connect(&engine, &QQmlApplicationEngine::objectCreationFailed,
                     qApp, &QCoreApplication::quit,
                     Qt::QueuedConnection);

    QObject::connect(&engine, &QQmlApplicationEngine::quit,
                     qApp, &QGuiApplication::quit,
                     Qt::QueuedConnection);


    qmlRegisterType<RobotSatQml>("RobotApp", 1, 0, "RobotMobileApp");

    QtSkAppCtrl skCtrl(argc, argv);
    skCtrl.init(5000, 250000);

    RobotSat *sat = new RobotSat;
    RobotSatQml *satQmlCtrl = new RobotSatQml(sat);
    satQmlCtrl->setSatellite(sat);
    satQmlCtrl->setRobotAddress("tcp:192.168.2.49");
    satQmlCtrl->setUserName("guest");
    satQmlCtrl->setPassword("password");

    engine.rootContext()->setContextProperty("sat", satQmlCtrl);
    engine.load(url);

    skCtrl.launch();
    return app.exec();
}
