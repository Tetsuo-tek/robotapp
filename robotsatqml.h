#ifndef ROBOTSATQML_H
#define ROBOTSATQML_H

#include "robotsat.h"
#include <qqml.h>

struct QmlFlowChannel {
    Q_GADGET

    Q_PROPERTY(SkFlowChanID chanID MEMBER chanID)
    Q_PROPERTY(QString flow_t MEMBER flow_t)
    Q_PROPERTY(QString chan_t MEMBER chan_t)
    Q_PROPERTY(QString data_t MEMBER data_t)
    Q_PROPERTY(QString name MEMBER name)
    Q_PROPERTY(QString hashID MEMBER hashID)

    public:
        QmlFlowChannel(SkFlowChannel *ch=nullptr)
        {
            if (!ch)
                return;

            chanID = ch->chanID;

            if (ch->chan_t == StreamingChannel)
            {
                flow_t = SkFlowProto::flowTypeToString(ch->flow_t);
                data_t = SkVariant::variantTypeName(ch->data_t);
                chan_t = "StreamingChannel";
            }

            else
                chan_t = "ServiceChannel";

            name = ch->name.c_str();
            hashID = ch->hashID.c_str();
        }

        SkFlowChanID chanID;
        QString flow_t;
        QString chan_t;
        QString data_t;
        QString name;
        QString hashID;

        //Q_SLOT void foo() { qDebug() << "foo"; }
};

Q_DECLARE_METATYPE(QmlFlowChannel)

class RobotSatQml extends QObject
{
    Q_OBJECT

    Q_PROPERTY(QString robotAddress READ robotAddress WRITE setRobotAddress NOTIFY robotAddressChanged)
    Q_PROPERTY(QString userName READ userName WRITE setUserName NOTIFY userNameChanged)
    Q_PROPERTY(QString password READ password WRITE setPassword NOTIFY passwordChanged)

    QML_ELEMENT

    QString m_robotAddress;
    QString m_userName;
    QString m_password;

    RobotSat *sat;

    public:
        explicit RobotSatQml(QObject *parent=nullptr);

        void setSatellite(RobotSat *satellite);

        void setRobotAddress(const QString &robotAddress);
        QString robotAddress();

        void setUserName(const QString &userName);
        QString userName();

        void setPassword(const QString &password);
        QString password();

    public slots:
        void open();
        void close();

        bool containsChannel(const QString &name);
        QmlFlowChannel channel(SkFlowChanID chanID);
        QmlFlowChannel channel(const QString &name);
        SkFlowChanID channelID(const QString &name);
        ULong channelsCount();

    private slots:
        void onReady();
        void onDisconnection();

    signals:
        void ready();
        void disconnected();

        void channelAdded(SkFlowChanID);
        void channelRemoved(SkFlowChanID);
        void channelHeaderSetup(SkFlowChanID);
        void channelPublishStartRequest(SkFlowChanID);
        void channelPublishStopRequest(SkFlowChanID);

        void robotAddressChanged();
        void userNameChanged();
        void passwordChanged();
};

#endif // ROBOTSATQML_H
