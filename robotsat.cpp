#include "robotsat.h"

RobotSat::RobotSat(QObject *parent) : QtFlowSat{parent}
{

}

void RobotSat::onFlowDataCome(SkFlowChannelData &chData)
{

}

void RobotSat::onChannelAdded(SkFlowChanID chanID)
{

}

void RobotSat::onChannelRemoved(SkFlowChanID chanID)
{

}

void RobotSat::onChannelHeaderSetup(SkFlowChanID chanID)
{

}

void RobotSat::onChannelPublishStartRequest(SkFlowChanID chanID)
{

}

void RobotSat::onChannelPublishStopRequest(SkFlowChanID chanID)
{

}

void RobotSat::onFastTick()
{

}

void RobotSat::onSlowTick()
{

}

void RobotSat::onOneSecTick()
{
    //QtObjectMessage("1 SEC: " << skApp->getLastConsumedTime() << " / " << skApp->getLastPulseElapsedTimeAverage());
}

void RobotSat::onDisconnection()
{
    QtObjectMessage("Disconnected");
}
