#ifndef ROBOTSAT_H
#define ROBOTSAT_H

#include <Core/System/Network/FlowNetwork/qtflowsat.h>

class RobotSat extends QtFlowSat
{
    Q_OBJECT

    public:
        explicit RobotSat(QObject *parent=nullptr);

    private:
        void onFlowDataCome(SkFlowChannelData &chData)          override;

        void onChannelAdded(SkFlowChanID chanID)                override;
        void onChannelRemoved(SkFlowChanID chanID)              override;
        void onChannelHeaderSetup(SkFlowChanID chanID)          override;

        void onChannelPublishStartRequest(SkFlowChanID chanID)  override;
        void onChannelPublishStopRequest(SkFlowChanID chanID)   override;

        void onFastTick()                                       override;
        void onSlowTick()                                       override;
        void onOneSecTick()                                     override;

        void onDisconnection()                                  override;
};

#endif // ROBOTSAT_H
