#include "robotsatqml.h"

RobotSatQml::RobotSatQml(QObject *parent) : QObject{parent}
{
    sat = nullptr;
    setObjectName("RobotSatQml");
}

void RobotSatQml::setRobotAddress(const QString &robotAddress)
{
    m_robotAddress = robotAddress;
    QtObjectMessage("Setup for sat-address url: " << m_robotAddress.toStdString());
    robotAddressChanged();
}

QString RobotSatQml::robotAddress()
{
    return m_robotAddress;
}

void RobotSatQml::setSatellite(RobotSat *satellite)
{
    sat = satellite;
    connect(sat, &RobotSat::ready,                      this, &RobotSatQml::onReady,                    Qt::DirectConnection);
    connect(sat, &RobotSat::disconnected,               this, &RobotSatQml::onDisconnection,            Qt::DirectConnection);
}

void RobotSatQml::setUserName(const QString &userName)
{
    m_userName = userName;
    QtObjectMessage("Setup for user name: " << m_userName.toStdString());
    userNameChanged();
}

QString RobotSatQml::userName()
{
    return m_userName;
}

void RobotSatQml::setPassword(const QString &password)
{
    m_password = password;
    QtObjectMessage("Setup for user password");
    passwordChanged();
}

QString RobotSatQml::password()
{
    return m_password;
}

void RobotSatQml::open()
{
    connect(sat, &RobotSat::channelAdded,                   this, &RobotSatQml::channelAdded,               Qt::DirectConnection);
    connect(sat, &RobotSat::channelRemoved,                 this, &RobotSatQml::channelRemoved,             Qt::DirectConnection);
    connect(sat, &RobotSat::channelHeaderSetup,             this, &RobotSatQml::channelHeaderSetup,         Qt::DirectConnection);
    connect(sat, &RobotSat::channelPublishStartRequest,     this, &RobotSatQml::channelPublishStartRequest, Qt::DirectConnection);
    connect(sat, &RobotSat::channelPublishStopRequest,      this, &RobotSatQml::channelPublishStopRequest,  Qt::DirectConnection);

    sat->setup("guest", "password", "tcp:192.168.2.49");
    sat->open();
}

void RobotSatQml::close()
{
    disconnect(sat, &RobotSat::channelAdded,                this, &RobotSatQml::channelAdded);
    disconnect(sat, &RobotSat::channelRemoved,              this, &RobotSatQml::channelRemoved);
    disconnect(sat, &RobotSat::channelHeaderSetup,          this, &RobotSatQml::channelHeaderSetup);
    disconnect(sat, &RobotSat::channelPublishStartRequest,  this, &RobotSatQml::channelPublishStartRequest);
    disconnect(sat, &RobotSat::channelPublishStopRequest,   this, &RobotSatQml::channelPublishStopRequest);

    if (sat->isOpen())
        sat->close();
}

bool RobotSatQml::containsChannel(const QString &name)
{
    SkString str(name.toStdString());
    return sat->containsChannel(str.c_str());
}

QmlFlowChannel RobotSatQml::channel(SkFlowChanID chanID)
{
    return QmlFlowChannel(sat->channel(chanID));
}

QmlFlowChannel RobotSatQml::channel(const QString &name)
{
    SkString str(name.toStdString());
    return QmlFlowChannel(sat->channel(str.c_str()));
}

SkFlowChanID RobotSatQml::channelID(const QString &name)
{
    SkString str(name.toStdString());
    return sat->channelID(str.c_str());
}

ULong RobotSatQml::channelsCount()
{
    return sat->channelsCount();
}

void RobotSatQml::onReady()
{
    // MAKE SOMETHING BEFORE TO SEND ON QML
    ready();
}

void RobotSatQml::onDisconnection()
{
    // MAKE SOMETHING BEFORE TO SEND ON QML
    disconnected();
}
