import QtCore
import QtQuick
import QtQuick.Controls
import QtQuick.Controls.Material
import QtQuick.Layouts

import RobotApp

ApplicationWindow {
    title: "RobotMobileApp"
    width: 640
    height: 360
    visible: true
    /*menuBar: MenuBar {
        Menu {
            title: qsTr("Application")

            Action {
                text: qsTr("&Connect")
                onTriggered: console.log("Open action triggered");
            }

            Action { text: qsTr("&Disconnect") }

            MenuSeparator {}

            Action {
                text: qsTr("&Quit")
                onTriggered: Qt.quit();
            }
        }
        Menu {
            title: qsTr("View")
        }
        Menu {
            title: qsTr("Publish")
        }
        Menu {
            title: qsTr("Subscribe")
        }
        Menu {
            title: qsTr("Help")
        }
    }

    header: Label {
        id: headerLabel
        text: view.currentItem.title
        horizontalAlignment: Text.AlignHCenter
    }*/

    SwipeView {
        id: view
        anchors.fill: parent
        currentIndex: 0

        onCurrentIndexChanged: {
            print("currentIndex changed to", currentIndex)
        }

        Page {
            id: homePage
            title: "Login"

            ColumnLayout {
                anchors.centerIn: parent

                GridLayout {
                    columns: 2

                    Label { text: "Robot address" }
                    TextField {
                        id: robotAddressField
                        text: sat.robotAddress
                        Layout.fillWidth: true
                    }

                    Label { text: "Username" }
                    TextField {
                        id: usernameField
                        text: sat.userName
                        Layout.fillWidth: true
                    }

                    Label { text: "Password" }
                    TextField {
                        id: passwordField
                        Layout.fillWidth: true
                        text: sat.password
                        echoMode: TextInput.Password
                    }
                }

                CheckBox {
                    Layout.fillWidth: true
                    text: "Remember me"
                }

                Button {
                    id: loginBtn
                    Layout.fillWidth: true
                    text: "Login"
                    onClicked: {
                        sat.robotAddress = robotAddressField.text
                        sat.userName = usernameField.text
                        sat.password = passwordField.text
                        sat.open()
                    }
                }

                Button {
                    id: logoutBtn
                    enabled: false
                    Layout.fillWidth: true
                    text: "Logout"
                    onClicked: sat.close()
                }
            }
        }

        Page {
            id: page2
            title: "View"

            /*Column {
                anchors.centerIn: parent
                spacing: 10

                Text {
                    text: "Page 2!"
                    font.pointSize: 20
                }

                Button {
                    text: "Go 0"
                    onClicked:  {
                        view.currentIndex = 0;
                    }
                }
            }*/
        }
    }

    /*PageIndicator {
        id: indicator

        count: view.count
        currentIndex: view.currentIndex

        anchors.bottom: view.bottom
        anchors.horizontalCenter: parent.horizontalCenter
    }*/

    // CONNECTIONs TO sat SIGNALs

    Connections {
        target: sat
        function onReady() {
            console.log("FlowSat is READY");
            robotAddressField.enabled = false;
            usernameField.enabled = false;
            passwordField.enabled = false;
            loginBtn.enabled = false;
            logoutBtn.enabled = true;
            view.currentIndex = 1;
        }
    }

    Connections {
        target: sat
        function onDisconnected() {
            console.log("FlowSat is DISCONNECTED");
            robotAddressField.enabled = true;
            usernameField.enabled = true;
            passwordField.enabled = true;
            loginBtn.enabled = true;
            logoutBtn.enabled = false;
            view.currentIndex = 0;
        }
    }

    Connections {
        target: sat
        function onChannelAdded(chanID) {
            var ch = sat.channel(chanID);

            if (ch.chan_t === "StreamingChannel")
                console.log("StreamingChannel ADDED -> ["
                            + "chanID: " + ch.chanID
                            + "; flow_t: " + ch.flow_t
                            + "; data_t: " + ch.data_t
                            + "; name: " + ch.name
                            + "; hashID: " + ch.hashID
                            + "]");

            else
                console.log("ServiceChannel ADDED -> ["
                            + "chanID: " + ch.chanID
                            + "; name: " + ch.name
                            + "; hashID: " + ch.hashID
                            + "]");
        }
    }

    Connections {
        target: sat
        function onChannelRemoved(chanID) {
            console.log("Channel REMOVED: " + chanID);
        }
    }
}
